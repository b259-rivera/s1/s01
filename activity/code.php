<?php 

$country = 'Philippines';
$city = 'Metro Manila';
$province = 'Timog Avenue, Quezon City';
$address = '3F Caswynn Bldg.';

function getFullAddress($country, $city, $province, $address) {

    $fullAddress = $address . ', ' . $province . ', ' . $city . ', ' . $country;
    return $fullAddress;
}

$fullAddress = getFullAddress($country, $city, $province, $address);

$countryB = 'Philippines';
$cityB = 'Metro Manila';
$provinceB = 'Buendia Avenue, Makati City';
$addressB = '3F Enzo Bldg.';


$fullAddressA = getFullAddress($countryB, $cityB, $provinceB, $addressB);

/* Activity 2 */
function getLetterGrade($grade) {
    if ($grade >= 98 && $grade <= 100) {
        return "$grade is equivalent to A+";
    } elseif ($grade >= 95 && $grade <= 97) {
        return "$grade is equivalent to A";
    } elseif ($grade >= 92 && $grade <= 94) {
        return "$grade is equivalent to A-";
    } elseif ($grade >= 89 && $grade <= 91) {
        return "$grade is equivalent to B+";
    } elseif ($grade >= 86 && $grade <= 88) {
        return "$grade is equivalent to B";
    } elseif ($grade >= 83 && $grade <= 85) {
        return "$grade is equivalent to B-";
    } elseif ($grade >= 80 && $grade <= 82) {
        return "$grade is equivalent to C+";
    } elseif ($grade >= 77 && $grade <= 79) {
        return "$grade is equivalent to C";
    } elseif ($grade >= 75 && $grade <= 76) {
        return "$grade is equivalent to C-";
    } else {
        return "$grade is equivalent to F";
    }
}

$gradeA = 87;
$gradeB = 94;
$gradeC = 74;


$letterGradeA = getLetterGrade($gradeA);
$letterGradeB = getLetterGrade($gradeB);
$letterGradeC = getLetterGrade($gradeC);